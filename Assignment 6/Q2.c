#include<stdio.h>
int f(int);

int main(void)
{
  int m, i = 0, n;

  printf("Give a Number:");
  scanf("%d", &m);

  printf("Fibonacci series terms are:\n");

  for (n = 1; n <= m; n++)
  {
    printf("%d\n", f(i));
    i++;
  }

  return 0;
}

int f(int m)
{
  if (m == 0 || m == 1)
    return m;
  else
    return (f(m-1) + f(m-2));
}
