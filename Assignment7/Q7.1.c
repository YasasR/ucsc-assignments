#include <stdio.h>

void rev();

void main(void)
{
    printf("Please enter a sentence: ");
    rev();
}

void rev()
{
    char x;
    scanf("%c", &x);
    if (x != '\n') {
        rev();
        printf("%c", x);
    }
}
