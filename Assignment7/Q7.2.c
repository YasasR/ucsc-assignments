#include <stdio.h>
int main(void) {
    char A[1000], x;
    int count = 0;

    printf("Enter any string: ");
    fgets(A, sizeof(A), stdin);

    printf("Enter any character, which you want to find the frequency: ");
    scanf("%c", &x);

    for (int i = 0; A[i] != '\0'; ++i) {
        if (x == A[i])
            ++count;
    }

    printf("Frequency of %c in this string = %d", x, count);
    return 0;
}
