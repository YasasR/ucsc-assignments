#include <stdio.h>
int main(void) {
    int x, y, c[100][100], d[100][100], sum[100][100], i, j;
    printf("Number of rows of the matrix : ");
    scanf("%d", &x);
    printf("Number of columns of matrix : ");
    scanf("%d", &y);

    printf("\nElements of 1st matrix:\n");
    for (i = 0; i < x; ++i)
        for (j = 0; j < y; ++j) {
            printf("Enter the element a%d%d: ", i + 1, j + 1);
            scanf("%d", &c[i][j]);
        }

    printf("Elements of 2nd matrix:\n");
    for (i = 0; i < x; ++i)
        for (j = 0; j < y; ++j) {
            printf("Enter the element a%d%d: ", i + 1, j + 1);
            scanf("%d", &d[i][j]);
        }


    for (i = 0; i < x; ++i)
        for (j = 0; j < y; ++j) {
            sum[i][j] = c[i][j] + d[i][j];
        }


    printf("\nSum of the matrices: \n");
    for (i = 0; i < x; ++i)
        for (j = 0; j < y; ++j) {
            printf("%d   ", sum[i][j]);
            if (j == y - 1) {
                printf("\n\n");
            }
        }

    return 0;
}
