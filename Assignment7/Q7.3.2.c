#include<stdio.h>

int main(void)
{
  int a, b, p, q, x, y, r, tot = 0;
  int matA[10][10], matB[10][10], matC[10][10];

  printf(" Enter the number of rows for the first matrix \n ");
  scanf("%d", &x);
  printf(" Enter the number of columns for the first matrix \n ");
  scanf("%d", &y);

  printf(" Elements are entered towards right side, first row to the last row : \n ");
  for (a = 0; a < x; a++)
    for (b = 0; b < y; b++)
      scanf("%d", &matA[a][b]);

  printf(" Enter the number of rows for the second matrix\n");
  scanf(" %d", &p);
  printf(" Enter the number of columns for the second matrix\n");
  scanf(" %d", &q);

  if (y != p)
    printf("These two matrices cannot be multiplied, the number of columns of the 1st matrix should be identical to the number of rows of the 2nd matrix. \n ");
  else
  {
    printf(" Elements are entered towards right side, first row to the last row \n ");

    for (a = 0; a < p; a++)
      for (b = 0; b < q; b++)
        scanf("%d", &matB[a][b] );

    for (a = 0; a < x; a++) {
      for (b = 0; b < q; b++) {
        for (r = 0; r < p; r++) {
          tot = tot + matA[a][r] * matB[r][b];
        }
        matC[a][b] = tot;
        tot = 0;
      }
    }

    printf(" Product of the given matrices is: \n ");
    for (a = 0; a < x; a++) {
      for (b = 0; b < q; b++)
        printf("%d \t", matC[a][b] );
      printf(" \n ");
    }
  }
  return 0;
}
